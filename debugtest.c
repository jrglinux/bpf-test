#include <stdio.h>

void test_func1(int num)
{
    printf("recv number %d\n", num);
}

int main(int argc, char *argv[])
{
    test_func1(2);
    return 0;
}
